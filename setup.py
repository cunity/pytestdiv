from setuptools import setup

VERSION = "0.3.2"

setup(
    name="pytestdiv",
    version=VERSION,
    description="A Pytest plugin to divide test execution for parallel runs",
    author="Ian Norton",
    author_email="inorton@gmail.com",
    url="https://gitlab.com/cunity/pytestdiv",
    py_modules=["pytestdiv"],
    install_requires=[
        "pytest",
    ],
    platforms=["any"],
    license="License :: OSI Approved :: MIT License",
    long_description="Split test collection and execution into approximately parallel groups",
    entry_points={
        "pytest11": ["pytestdiv = pytestdiv"]
    },
    classifiers=["Framework :: Pytest"],
)

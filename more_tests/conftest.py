def pytest_addoption(parser, pluginmanager):
    # this option will never actually get added, pytest_addoption() only works for top level conftest.py and pre-loaded
    # plugins executed before test collection
    parser.addoption("--foo", type=str)
